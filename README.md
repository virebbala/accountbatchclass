# README #

Steps:

1. Use GenericBatch.cls to create a new apex class in your org.
2. Run the below code in the Anonymous block of developer console to execute the above batch class which 
   performs an update for the given sObject, Query and fields to be updated.

    //Field which needs to be updated with its Data type
	Map<String, String> accountFields = new Map<String, String>();
	accountFields.put('Enterprise_Account_Status__c', 'String');

	// Fields Values
	Map<String, String> accountFieldValues = new Map<String, String>();
	accountFieldValues.put('Enterprise_Account_Status__c', 'Bronze');

	String whereClause = ' RecordType.DeveloperName = \'Customer_Account\' and Enterprise_Account_Status__c != \'Bronze\'';
	GenericBatch accountBatch = new GenericBatch('Account', whereClause, accountFields, accountFieldValues);

	Database.executeBatch(accountBatch);
	
3. You can add field names and its datatype to accountFields map, add what values to be updated for the fields in accountFieldValues map, whereClause to filter the query 
   and when calling the batch class mention the object API name.
   
4. we can also create a scheduler class with the above code snippet to call the batch class at a given frequency.

5. The above code snippet can be changed for different sObjects and for updating different fields of that object.